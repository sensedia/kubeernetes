#!/bin/bash
# Reference https://kubernetes.io/docs/reference/kubectl/cheatsheet/
# permanently save the namespace for all subsequent kubectl commands in that context.
echo Setting default namespace to $1
kubectl config set-context --current --namespace=$1